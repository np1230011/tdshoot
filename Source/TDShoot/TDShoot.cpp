// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShoot.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDShoot, "TDShoot" );

DEFINE_LOG_CATEGORY(LogTDShoot)
 