// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShootCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"

ATDShootCharacter::ATDShootCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//UGameplayStatics::SpawnSound2D()
}

void ATDShootCharacter::Tick(float DeltaSeconds)
{

    Super::Tick(DeltaSeconds);
}

void ATDShootCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon();

}

void ATDShootCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(NewInputComponent);
	//Forw Move
	EnhancedInputComponent->BindAction(MoveCharForw, ETriggerEvent::Triggered, this, &ATDShootCharacter::EnhancedInputForwardMove);
	EnhancedInputComponent->BindAction(MoveCharBack, ETriggerEvent::Triggered, this, &ATDShootCharacter::EnhancedInputBackMove);
	EnhancedInputComponent->BindAction(MoveCharRight, ETriggerEvent::Triggered, this, &ATDShootCharacter::EnhancedInputRightMove);
	EnhancedInputComponent->BindAction(MoveCharLeft, ETriggerEvent::Triggered, this, &ATDShootCharacter::EnhancedInputLeftMove);
	EnhancedInputComponent->BindAction(RKM, ETriggerEvent::Triggered, this, &ATDShootCharacter::EnhancedInputRKM);

	EnhancedInputComponent->BindAction(MoveCharWalk, ETriggerEvent::Triggered, this, &ATDShootCharacter::EnhancedInputWalk);
	EnhancedInputComponent->BindAction(MoveCharWalk, ETriggerEvent::Completed, this, &ATDShootCharacter::EnhancedInputRun);

	EnhancedInputComponent->BindAction(MoveCharSprint, ETriggerEvent::Triggered, this, &ATDShootCharacter::EnhancedInputSprint);
	EnhancedInputComponent->BindAction(MoveCharSprint, ETriggerEvent::Completed, this, &ATDShootCharacter::EnhancedInputRun);

	EnhancedInputComponent->BindAction(CameraWheelScroll, ETriggerEvent::Started, this, &ATDShootCharacter::EnhancedInputCameraScroll);
	
	EnhancedInputComponent->BindAction(LKM, ETriggerEvent::Triggered, this, &ATDShootCharacter::EnhancedInputLKMStarted);
	EnhancedInputComponent->BindAction(LKM, ETriggerEvent::Completed, this, &ATDShootCharacter::EnhancedInputLKMCompleted);
}

void ATDShootCharacter::EnhancedInputForwardMove(const FInputActionValue& Value)
{
	AddMovementInput(FVector(7.0f,0.0f,0.0f),1.0f);
}

void ATDShootCharacter::EnhancedInputBackMove(const FInputActionValue& Value)
{
	AddMovementInput(FVector(7.0f, 0.0f, 0.0f), -1.0f);
}

void ATDShootCharacter::EnhancedInputRightMove(const FInputActionValue& Value)
{
	AddMovementInput(FVector(0.0f, 7.0f, 0.0f), -1.0f);
}

void ATDShootCharacter::EnhancedInputLeftMove(const FInputActionValue& Value)
{
	AddMovementInput(FVector(0.0f, 7.0f, 0.0f), 1.0f);
}

void ATDShootCharacter::EnhancedInputKM(const FInputActionValue& Value)
{
	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		FHitResult ResultHit;
		MyController->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, ResultHit);
		//UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location);
		SetActorRotation(FQuat(FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw, 0.0f)));
		IsCharAim = 1;
		ChangeMovementState(EMovementState::Aim_State);
	}
}

void ATDShootCharacter::EnhancedInputWalk(const FInputActionValue& Value)
{
	ChangeMovementState(EMovementState::Walk_State);

}

void ATDShootCharacter::EnhancedInputRun(const FInputActionValue& Value)
{
	ChangeMovementState(EMovementState::Run_State);
	IsCharAim = 0;
}

void ATDShootCharacter::EnhancedInputSprint(const FInputActionValue& Value)
{
	if (IsCharAim)
	{ }
	else if (StaminaM > 0)
	{
		ChangeMovementState(EMovementState::Sprint_State);
	}
	else
	{
		ChangeMovementState(EMovementState::Run_State);
	}
}

void ATDShootCharacter::EnhancedInputCameraScroll(const FInputActionValue& Value)
{
	if (IsSlideDone)
	{
		if (Value.Get<float>() > 0)
		{
			if (CameraBoom->TargetArmLength > CameraMinimum)
			{
				IsSlideUp = 0;
				IsSlideDone = 0;
				GetWorld()->GetTimerManager().SetTimer(TimeHandle, this, &ATDShootCharacter::CameraSlideSmoothTick, TimerStepScroll, true);
			}
		}
		else
		{
			if (CameraBoom->TargetArmLength < CameraMaximum)
			{
				IsSlideUp = 1;
				IsSlideDone = 0;
				GetWorld()->GetTimerManager().SetTimer(TimeHandle, this, &ATDShootCharacter::CameraSlideSmoothTick, TimerStepScroll, true);
			}
		}
	}
}

void ATDShootCharacter::EnhancedInputLKMStarted(const FInputActionValue& Value)
{
	AttackCharEvent(true);
}

void ATDShootCharacter::EnhancedInputLKMCompleted(const FInputActionValue& Value)
{
	AttackCharEvent(false);
}


void ATDShootCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		IsCharAim = 1;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
			ResSpeed = MovementInfo.RunSpeed;
			IsCharAim = 0;
		break;
	case EMovementState::Sprint_State:
		if (IsCharAim)
		{
			ResSpeed = MovementInfo.AimSpeed;
			IsCharAim = 1;
		}
		else
		{
			ResSpeed = MovementInfo.SprintSpeed;
			IsCharAim = 0;
		}
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	TecSpeed = ResSpeed;
}

void ATDShootCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	
		MovementState = NewMovementState;
		CharacterUpdate();
	
}

void ATDShootCharacter::CameraSlideSmoothTick()
{
	CurrentSlideDist += HeightCameraDistance;
	if (IsSlideUp)
	{
		CameraBoom->TargetArmLength += HeightCameraDistance;
	}
	else
	{
		CameraBoom->TargetArmLength -= HeightCameraDistance;
	}
	if (CurrentSlideDist >= HeightCameraChangeDist)
	{
		CurrentSlideDist = 0.0f;
		IsSlideDone = 1;
		GetWorld()->GetTimerManager().ClearTimer(TimeHandle);
	}

}

void ATDShootCharacter::InitWeapon()
{
	if (InitWeaponClass)
	{
		FVector SpawnLocation = FVector(0);
		FRotator SpawnRotation = FRotator(0);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = GetInstigator();

		AWeaponDefault* MyWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(InitWeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (MyWeapon)
		{
			FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
			MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
			CurrentWeapon = MyWeapon;
		}
	}
}

AWeaponDefault* ATDShootCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDShootCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* MyWeapon = nullptr;
	MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATDSootCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
}





