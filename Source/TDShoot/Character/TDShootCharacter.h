// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputAction.h"
#include "InputMappingContext.h"
#include "TDShoot/FuncLibrary/Type.h"
#include "TDShoot/WeaponDefault.h"
#include "TDShootCharacter.generated.h"

UCLASS(Blueprintable)
class ATDShootCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDShootCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	//Called for Forward Move
	void EnhancedInputForwardMove(const FInputActionValue& Value);
	void EnhancedInputBackMove(const FInputActionValue& Value);
	void EnhancedInputRightMove(const FInputActionValue& Value);
	void EnhancedInputLeftMove(const FInputActionValue& Value);
	void EnhancedInputRKM(const FInputActionValue& Value);
	void EnhancedInputWalk(const FInputActionValue& Value);
	void EnhancedInputRun(const FInputActionValue& Value);
	void EnhancedInputSprint(const FInputActionValue& Value);
	void EnhancedInputCameraScroll(const FInputActionValue& Value);
	void EnhancedInputLKMStarted(const FInputActionValue& Value);
	void EnhancedInputLKMCompleted(const FInputActionValue& Value);

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

private:

	void OnInputStarted();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* InputMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveCharForw;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveCharRight;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveCharBack;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveCharLeft;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveCharWalk;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveCharSprint;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* CameraWheelScroll;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LKM;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* RKM;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;
	UPROPERTY()
	float TimerStepScroll = 0.001f;
	UPROPERTY()
	bool IsSlideDone = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CameraMinimum = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CameraMaximum = 3000.0f;
	UPROPERTY()
	float CurrentSlideDist = 0.0f;
	UPROPERTY()
	float HeightCameraDistance = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HeightCameraChangeDist = 100.0f;
	UPROPERTY()
	bool IsSlideUp = 0;
	UPROPERTY()
	FTimerHandle TimeHandle;
	UPROPERTY()
	bool IsCharAim = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StaminaM = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TecSpeed = 0.0f;

	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	TSubclassOf<AWeaponDefault> InitWeaponClass = nullptr;

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState(EMovementState NewMovementState);

	UFUNCTION()
	void CameraSlideSmoothTick();

	UFUNCTION()
	void InitWeapon();

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);


};

