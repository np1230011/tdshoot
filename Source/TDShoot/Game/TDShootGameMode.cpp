// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShootGameMode.h"
#include "TDShootPlayerController.h"
#include "TDShoot/Character/TDShootCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDShootGameMode::ATDShootGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDShootPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprint/Character/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}