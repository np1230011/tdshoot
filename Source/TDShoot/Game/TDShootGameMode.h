// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDShootGameMode.generated.h"

UCLASS(minimalapi)
class ATDShootGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDShootGameMode();
};



